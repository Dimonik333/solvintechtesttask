﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightning : MonoBehaviour
{
    private Transform _transform;
    public Transform GetTransform() { return _transform; }

    public GameObject RemoveEffectPrefab;
    public GameObject ExplosionEffectPrefab;

    public event Action<Lightning> Destroyed;

    private void Awake()
    {
        _transform = transform;
    }

    public void Remove(Vector3 direction)
    {
        var effect = Instantiate(RemoveEffectPrefab, _transform.position, Quaternion.identity, _transform.parent);
        Destroyed?.Invoke(this);
        Destroy(gameObject);
    }

    public void Explode()
    {
        var effect = Instantiate(ExplosionEffectPrefab, _transform.position, Quaternion.identity, _transform.parent);
        Destroyed?.Invoke(this);
        Destroy(gameObject);
    }
}
