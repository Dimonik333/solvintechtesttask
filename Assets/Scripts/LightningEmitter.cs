﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningEmitter : MonoBehaviour
{
    private float _elapsedTime;
    private List<Lightning> _lightnings = new List<Lightning>();
    private List<Lightning> destroyedLightnings = new List<Lightning>();
    public Lightning LightiningPrefab;

    [Space]
    public float SpawnDelay;
    public Transform[] SpawnPositions;
    public Transform Space;

    [Space]
    public float MoveSpeed;
    public Vector3 MoveDirection;
    public Transform Target;

    public float Multiplier { get; set; } = 1f;

    void Update()
    {
        if (_elapsedTime > SpawnDelay / Multiplier)
        {
            _elapsedTime -= SpawnDelay / Multiplier;
            SpawnObject();
        }
        _elapsedTime += Time.deltaTime;

        MoveLightnings();
    }

    private void SpawnObject()
    {
        int spawnPositionIndex = Random.Range(0, SpawnPositions.Length);
        var spawnPosition = SpawnPositions[spawnPositionIndex];

        var lighting = Instantiate(LightiningPrefab, spawnPosition.position, Quaternion.identity, Space);
        lighting.Destroyed += OnLightingDestroyed;
        _lightnings.Add(lighting);
    }

    private void OnLightingDestroyed(Lightning lightning)
    {
        lightning.Destroyed -= OnLightingDestroyed;
        destroyedLightnings.Add(lightning);
    }

    private void MoveLightnings()
    {
        foreach (var lightning in _lightnings)
        {
            var lt = lightning.GetTransform();
            lt.position += MoveDirection * MoveSpeed * Multiplier * Time.deltaTime;

            if (lt.position.y > Target.position.y)
                lightning.Explode();
        }
    }

    private void LateUpdate()
    {
        foreach (var lightning in destroyedLightnings)
            _lightnings.Remove(lightning);

        destroyedLightnings.Clear();
    }

    private void OnDrawGizmosSelected()
    {
        for (int i = 0; i < SpawnPositions.Length; i++)
        {
            var fromPoint = SpawnPositions[i].position;
            var toPoint = fromPoint;
            toPoint.y = Target.position.y;

            Gizmos.DrawLine(fromPoint, toPoint);
        }
    }

}
