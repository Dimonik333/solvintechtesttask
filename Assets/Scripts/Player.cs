﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    protected Vector2 _startPosition;
    protected Vector2 _lastPosition;

    public Camera MainCamera;

    void Update()
    {
        if (Input.touchCount > 0)
        {

            Touch touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    _startPosition = touch.position;
                    _lastPosition = touch.position;
                    break;
                case TouchPhase.Moved:
                    _lastPosition = touch.position;
                    break;
                case TouchPhase.Ended:
                    _lastPosition = touch.position;
                    break;
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            _startPosition = Input.mousePosition;
            _lastPosition = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            _lastPosition = Input.mousePosition;
            Raycast();
        }
    }

    private void Raycast()
    {
        var startPoint = MainCamera.ScreenToWorldPoint(_startPosition);
        var endPoint = MainCamera.ScreenToWorldPoint(_lastPosition);
        var direction = endPoint - startPoint;
        Debug.DrawRay(startPoint, direction);

        var hit = Physics2D.Raycast(startPoint, direction, direction.magnitude);
        if (hit.collider != null)
        {
            var lightning = hit.collider.GetComponent<Lightning>();
            if (lightning != null)
            {
                lightning.Remove(direction);
            }
            Debug.Log("HIT- " + hit.collider.gameObject.name);
        }

    }
}
