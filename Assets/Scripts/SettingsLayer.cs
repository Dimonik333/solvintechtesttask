﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsLayer : MonoBehaviour
{
    public Slider MultiplierSlider;
    public Text MultiplierValueText;

    public LightningEmitter Emitter;

    private void Start()
    {
        OnValueChanged(MultiplierSlider.value);
    }

    private void OnEnable()
    {
        MultiplierSlider.onValueChanged.AddListener(OnValueChanged);
    }

    private void OnDisable()
    {
        MultiplierSlider.onValueChanged.RemoveListener(OnValueChanged);
    }

    private void OnValueChanged(float value)
    {
        MultiplierValueText.text = value.ToString();
        Emitter.Multiplier = value;
    }
}
